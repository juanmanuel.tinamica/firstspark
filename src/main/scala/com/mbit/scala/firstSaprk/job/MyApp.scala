package com.mbit.scala.firstSaprk.job

import com.mbit.scala.firstSaprk.functions.functions._
import com.mbit.scala.firstSaprk.functions.transforms._
import com.mbit.scala.firstSaprk.model.Valores
import org.apache.spark.sql.functions.col

object MyApp extends SparkSessionWrapper {
  def main(args: Array[String]): Unit = {
    import spark.sqlContext.implicits._

    val primerDF = (1 to 10).map(ran => Valores(ran,s"nombre $ran")).toDF

    primerDF
      .withColumn("actual", isEven(col("valor")))
      .happyData
      .show
  }
}
