package com.mbit.scala.firstSaprk.functions

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.lit

object transforms {

  implicit class dataFranes(val s: DataFrame) {
    def happyData: DataFrame = {
      s.withColumn("happy", lit("data is fun"))
    }
  }

}
